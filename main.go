package main

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/mania25/gin-codebase-mvc/configs"
	"gitlab.com/mania25/gin-codebase-mvc/src/routes"
	"gitlab.com/mania25/gin-codebase-mvc/src/utils/databases"

	"github.com/gin-gonic/gin"
)

func initialize(engine *gin.Engine, cfg *configs.RuntimeConfig) {
	db, err := databases.NewPostgresql(
		databases.PostgresConfig{
			DSN:    cfg.POSTGRES.DSN,
			User:   cfg.POSTGRES.USER,
			Pass:   cfg.POSTGRES.PASS,
			DBName: cfg.POSTGRES.DB,
		},
	)

	if err != nil {
		fmt.Println(
			fmt.Errorf("Database connection failed: %s", err.Error()),
		)
		return
	}

	errPingDB := db.Ping()

	if errPingDB != nil {
		fmt.Println(
			fmt.Errorf("Failed to ping database server: %s", errPingDB.Error()),
		)
		return
	}

	fmt.Println("Connected to Postgresql DB")

	es, errConnectingES := databases.NewElastic(
		&databases.ElasticConfig{
			DSN:            cfg.ELASTICSEARCH.DSN,
			User:           cfg.ELASTICSEARCH.USER,
			Pass:           cfg.ELASTICSEARCH.PASS,
			SetHealthcheck: true,
			SetSniff:       true,
		},
	)

	if errConnectingES != nil {
		fmt.Println(
			fmt.Errorf("Elastic Search connection failed: %s", errConnectingES.Error()),
		)
		return
	}

	if es != nil {
		if es.IsRunning() {
			fmt.Printf("Connected to Elastic Search Instance \n")
		}
	}

	cassandra, errInitiateCassandraConnection := databases.NewCassandra(
		databases.CassandraConfig{
			ClusterDSN:  strings.Split(cfg.CASSANDRA.DSN, ","),
			Port:        cfg.CASSANDRA.PORT,
			Environment: os.Getenv("MODE"),
			Keyspace:    cfg.CASSANDRA.KEYSPACE,
		},
	)

	if errInitiateCassandraConnection != nil {
		fmt.Println(
			fmt.Errorf("Cassandra connection failed: %s \n", errInitiateCassandraConnection.Error()),
		)
		return
	}

	fmt.Print("Connected to Cassandra DB \n\n")

	routes.InitRoutes(engine, cfg, db, es, cassandra)

	engine.Run(
		fmt.Sprintf(":%d", cfg.PORT),
	)
}

func main() {
	cfg := configs.GetConfig()

	if cfg != nil {
		engine := gin.Default()

		if os.Getenv("MODE") == "production" {
			gin.SetMode(gin.ReleaseMode)
		}

		initialize(engine, cfg)
	}
}
