package routes

import (
	"database/sql"
	"log"

	"gitlab.com/mania25/gin-codebase-mvc/configs"
	DefaultController "gitlab.com/mania25/gin-codebase-mvc/src/controllers/defaultController"

	"github.com/gocql/gocql"
	"github.com/olivere/elastic"

	"github.com/gin-gonic/gin"
)

func InitRoutes(engine *gin.Engine, cfg *configs.RuntimeConfig, postgre *sql.DB, elastic *elastic.Client, cassandra *gocql.Session) {
	defaultController, errInitiateDefaultController := DefaultController.NewDefaultController(
		DefaultController.SetRuntimeConfig(cfg),
		DefaultController.SetPostgre(postgre),
		DefaultController.SetElastic(elastic),
		DefaultController.SetCassandra(cassandra),
	)

	if errInitiateDefaultController != nil {
		log.Println("[error] ", errInitiateDefaultController)
	}

	engine.GET("/", defaultController.GetAPIVersion)
}
