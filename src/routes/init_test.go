package routes

import (
	"database/sql"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/gocql/gocql"
	"github.com/olivere/elastic"
	"gitlab.com/mania25/gin-codebase-mvc/configs"
)

func TestInitRoutes(t *testing.T) {
	mockConfig := &configs.RuntimeConfig{}
	mockPostgre := &sql.DB{}
	mockElastic := &elastic.Client{}
	mockCassandra := &gocql.Session{}

	type args struct {
		engine    *gin.Engine
		cfg       *configs.RuntimeConfig
		postgre   *sql.DB
		elastic   *elastic.Client
		cassandra *gocql.Session
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
		{
			name: "Test routes init with all params valid.",
			args: args{
				cfg:       mockConfig,
				postgre:   mockPostgre,
				elastic:   mockElastic,
				cassandra: mockCassandra,
			},
		},
		{
			name: "Test routes init with one params invalid.",
			args: args{
				cfg:       nil,
				postgre:   mockPostgre,
				elastic:   mockElastic,
				cassandra: mockCassandra,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.args.engine = gin.New()
			InitRoutes(tt.args.engine, tt.args.cfg, tt.args.postgre, tt.args.elastic, tt.args.cassandra)
		})
	}
}
