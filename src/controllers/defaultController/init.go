package defaultController

import (
	"database/sql"
	"fmt"

	"gitlab.com/mania25/gin-codebase-mvc/configs"

	"github.com/gocql/gocql"
	"github.com/olivere/elastic"
)

func NewDefaultController(options ...DefaultControllerOptionFunc) (instance *DefaultControllerInstance, err error) {
	instance = &DefaultControllerInstance{
		cassandraClient: nil,
		elasticClient:   nil,
		postgreClient:   nil,
	}

	for _, option := range options {
		if errParseOption := option(instance); errParseOption != nil {
			return nil, errParseOption
		}
	}

	return
}

func SetRuntimeConfig(cfg *configs.RuntimeConfig) DefaultControllerOptionFunc {
	return func(instance *DefaultControllerInstance) (err error) {
		if cfg != nil {
			instance.runtimeConfig = cfg
		} else {
			err = fmt.Errorf("[error] Runtime config must not nil")
		}
		return
	}
}

func SetCassandra(cassandra *gocql.Session) DefaultControllerOptionFunc {
	return func(instance *DefaultControllerInstance) (err error) {
		if cassandra != nil {
			instance.cassandraClient = cassandra
		} else {
			err = fmt.Errorf("[error] Cassandra connection must not nil")
		}
		return
	}
}

func SetElastic(elastic *elastic.Client) DefaultControllerOptionFunc {
	return func(instance *DefaultControllerInstance) (err error) {
		if elastic != nil {
			instance.elasticClient = elastic
		} else {
			err = fmt.Errorf("[error] Elastic connection must not nil")
		}
		return
	}
}

func SetPostgre(postgre *sql.DB) DefaultControllerOptionFunc {
	return func(instance *DefaultControllerInstance) (err error) {
		if postgre != nil {
			instance.postgreClient = postgre
		} else {
			err = fmt.Errorf("[error] Postgre connection must not nil")
		}
		return
	}
}
