package defaultController

import (
	"database/sql"

	"gitlab.com/mania25/gin-codebase-mvc/configs"

	"github.com/gocql/gocql"
	"github.com/olivere/elastic"
)

type DefaultControllerOptionFunc func(*DefaultControllerInstance) error

type (
	DefaultControllerInstance struct {
		runtimeConfig   *configs.RuntimeConfig
		elasticClient   *elastic.Client
		postgreClient   *sql.DB
		cassandraClient *gocql.Session
	}
)
