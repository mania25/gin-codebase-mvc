package defaultController

import (
	"database/sql"
	"reflect"
	"testing"

	"github.com/gocql/gocql"
	"github.com/olivere/elastic"
	"gitlab.com/mania25/gin-codebase-mvc/configs"
)

func TestNewDefaultController(t *testing.T) {
	mockConfig := &configs.RuntimeConfig{}
	mockPostgre := &sql.DB{}
	mockElastic := &elastic.Client{}
	mockCassandra := &gocql.Session{}

	type args struct {
		options []DefaultControllerOptionFunc
	}
	tests := []struct {
		name         string
		args         args
		wantInstance *DefaultControllerInstance
		wantErr      bool
	}{
		// TODO: Add test cases.
		{
			name: "Test init default controller with all params correct",
			args: args{
				options: []DefaultControllerOptionFunc{
					SetRuntimeConfig(mockConfig),
					SetPostgre(mockPostgre),
					SetElastic(mockElastic),
					SetCassandra(mockCassandra),
				},
			},
			wantInstance: &DefaultControllerInstance{
				runtimeConfig:   mockConfig,
				postgreClient:   mockPostgre,
				elasticClient:   mockElastic,
				cassandraClient: mockCassandra,
			},
			wantErr: false,
		},
		{
			name: "Test init default controller with runtime config params nill",
			args: args{
				options: []DefaultControllerOptionFunc{
					SetRuntimeConfig(nil),
					SetPostgre(mockPostgre),
					SetElastic(mockElastic),
					SetCassandra(mockCassandra),
				},
			},
			wantInstance: nil,
			wantErr:      true,
		},
		{
			name: "Test init default controller with postgre instance params nill",
			args: args{
				options: []DefaultControllerOptionFunc{
					SetRuntimeConfig(mockConfig),
					SetPostgre(nil),
					SetElastic(mockElastic),
					SetCassandra(mockCassandra),
				},
			},
			wantInstance: nil,
			wantErr:      true,
		},
		{
			name: "Test init default controller with elastic instance params nill",
			args: args{
				options: []DefaultControllerOptionFunc{
					SetRuntimeConfig(mockConfig),
					SetPostgre(mockPostgre),
					SetElastic(nil),
					SetCassandra(mockCassandra),
				},
			},
			wantInstance: nil,
			wantErr:      true,
		},
		{
			name: "Test init default controller with cassandra instance params nill",
			args: args{
				options: []DefaultControllerOptionFunc{
					SetRuntimeConfig(mockConfig),
					SetPostgre(mockPostgre),
					SetElastic(mockElastic),
					SetCassandra(nil),
				},
			},
			wantInstance: nil,
			wantErr:      true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotInstance, err := NewDefaultController(tt.args.options...)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewDefaultController() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotInstance, tt.wantInstance) {
				t.Errorf("NewDefaultController() = %v, want %v", gotInstance, tt.wantInstance)
			}
		})
	}
}
