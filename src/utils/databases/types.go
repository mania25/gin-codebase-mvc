package databases

type (
	CassandraConfig struct {
		ClusterDSN  []string
		Keyspace    string
		Environment string
		Port        int
	}

	ElasticConfig struct {
		DSN            string
		User           string
		Pass           string
		SetSniff       bool
		SetHealthcheck bool
	}

	PostgresConfig struct {
		DSN    string
		User   string
		Pass   string
		Port   int
		DBName string
	}
)
