package databases

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

func NewPostgresql(cfg PostgresConfig) (*sql.DB, error) {
	var connStr string
	connStr = fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.DSN,
		cfg.User,
		cfg.Pass,
		cfg.DBName)

	return sql.Open("postgres", connStr)
}
