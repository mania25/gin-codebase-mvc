package databases

import (
	"testing"
)

func TestNewCassandra(t *testing.T) {
	testCase := []struct {
		name    string
		args    CassandraConfig
		wantErr bool
	}{
		{
			name: "Test init cassandra connection with full args params, env devel",
			args: CassandraConfig{
				ClusterDSN:  []string{"localhost"},
				Port:        9042,
				Environment: "development",
			},
			wantErr: false,
		},
		{
			name: "Test init cassandra connection with port not assigned in params",
			args: CassandraConfig{
				ClusterDSN:  []string{"localhost"},
				Environment: "development",
			},
			wantErr: false,
		},
		{
			name: "Test init cassandra connection with invalid hostname",
			args: CassandraConfig{
				ClusterDSN:  []string{""},
				Port:        9042,
				Environment: "development",
			},
			wantErr: true,
		},
	}

	for _, tt := range testCase {
		t.Run(tt.name, func(t *testing.T) {
			_, errInitCassandra := NewCassandra(tt.args)
			if (errInitCassandra != nil) != tt.wantErr {
				t.Errorf("NewCassandra() error = %v, wantErr %v", errInitCassandra, tt.wantErr)
				return
			}
		})
	}
}
