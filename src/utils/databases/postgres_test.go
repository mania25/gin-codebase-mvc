package databases

import (
	"testing"

	_ "github.com/lib/pq"
)

func TestNewPostgresql(t *testing.T) {
	type args struct {
		cfg PostgresConfig
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "Test postgresql connection with full params",
			args: args{
				cfg: PostgresConfig{
					DSN:    "localhost",
					User:   "root",
					Pass:   "250895230936",
					DBName: "test",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := NewPostgresql(tt.args.cfg)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewPostgresql() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
