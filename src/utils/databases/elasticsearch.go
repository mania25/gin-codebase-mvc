package databases

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/olivere/elastic"
)

func NewElastic(cfg *ElasticConfig) (client *elastic.Client, err error) {
	ctx := context.Background()

	dsnURL := strings.Split(cfg.DSN, ",")

	client, err = elastic.NewClient(
		elastic.SetURL(dsnURL...),
		elastic.SetBasicAuth(cfg.User, cfg.Pass),
		elastic.SetSniff(cfg.SetSniff),
		elastic.SetHealthcheck(cfg.SetHealthcheck),
		elastic.SetHealthcheckInterval(1*time.Second),
		elastic.SetErrorLog(log.New(os.Stderr, "ELASTIC ", log.LstdFlags)),
	)

	if err != nil {
		return nil, err
	}

	for _, valueDSN := range dsnURL {
		esInfo, esResponseCode, errGetESInfo := client.Ping(valueDSN).Do(ctx)

		if errGetESInfo != nil {
			log.Printf("Failed to get info from Elastic Search Instance: %s \n", errGetESInfo.Error())
		} else {
			fmt.Printf("Elasticsearch returned with code %d and version %s\n", esResponseCode, esInfo.Version.Number)
		}
	}

	return client, err
}
