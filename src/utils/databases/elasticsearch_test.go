package databases

import (
	"testing"
)

func TestNewElastic(t *testing.T) {
	type args struct {
		cfg *ElasticConfig
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "Test elastic search connection with full arg params",
			args: args{
				cfg: &ElasticConfig{
					DSN: "http://localhost:9200",
				},
			},
			wantErr: false,
		},
		{
			name: "Test elastic search connection with invalid hosts",
			args: args{
				cfg: &ElasticConfig{
					DSN: "http://localhost:9201",
				},
			},
			wantErr: false,
		},
		{
			name: "Test elastic search connection with empty hosts",
			args: args{
				cfg: &ElasticConfig{
					DSN: "",
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := NewElastic(tt.args.cfg)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewElastic() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
