package configs

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/fsnotify/fsnotify"

	"github.com/spf13/viper"
	_ "github.com/spf13/viper/remote"
)

// InitConfig is a function to initialize service config
func GetConfig() *RuntimeConfig {
	var runtimeConf RuntimeConfig

	// alternatively, you can create a new viper instance.
	var runtime_viper = viper.New()

	runtime_viper.AddRemoteProvider("consul", "127.0.0.1:8500", fmt.Sprintf("/service/research/uri/%s.toml", os.Getenv("MODE")))
	runtime_viper.SetConfigType("toml") // because there is no file extension in a stream of bytes, supported extensions are "json", "toml", "yaml", "yml", "properties", "props", "prop"

	// read from remote config the first time.
	err := runtime_viper.ReadRemoteConfig()

	if err != nil { // Handle errors reading the config file
		fmt.Printf("Fatal error remote config file: %s \n", err)
		runtimeConfFile := readConfigFromFile()
		return runtimeConfFile
	}

	// unmarshal config
	runtime_viper.Unmarshal(&runtimeConf)

	// open a goroutine to watch remote changes forever
	go func() {
		for {
			time.Sleep(time.Second * 1) // delay after each request

			// currently, only tested with etcd support
			err := runtime_viper.WatchRemoteConfig()
			if err != nil {
				log.Printf("unable to read remote config: %v", err)
				continue
			}

			// unmarshal new config into our runtime config struct. you can also use channel
			// to implement a signal to notify the system of the changes
			runtime_viper.Unmarshal(&runtimeConf)
		}
	}()

	return &runtimeConf
}

func readConfigFromFile() *RuntimeConfig {
	var runtimeConf RuntimeConfig

	// alternatively, you can create a new viper instance.
	viper.SetConfigName(os.Getenv("MODE")) // name of config file (without extension)
	viper.SetConfigType("toml")            // because there is no file extension in a stream of bytes, supported extensions are "json", "toml", "yaml", "yml", "properties", "props", "prop"
	viper.AddConfigPath("./configs/")      // optionally look for config in the working directory

	// read from config the first time.
	err := viper.ReadInConfig()

	if err != nil { // Handle errors reading the config file
		fmt.Printf("Fatal error config file: %s \n", err)
		return nil
	}

	// unmarshal config
	viper.Unmarshal(&runtimeConf)

	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("config changed", e.Name)

		err := viper.ReadInConfig()

		if err != nil { // Handle errors reading the config file
			fmt.Printf("Fatal error config file: %s \n", err)
		}

		// unmarshal config
		viper.Unmarshal(&runtimeConf)
	})

	return &runtimeConf
}
