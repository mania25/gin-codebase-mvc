package configs

type (
	RuntimeConfig struct {
		PORT          int
		POSTGRES      postgresConfig
		ELASTICSEARCH *elasticSearchConfig
		CASSANDRA     cassandraConfig
	}

	postgresConfig struct {
		DSN  string
		USER string
		PASS string
		DB   string
	}

	elasticSearchConfig struct {
		DSN   string
		USER  string
		PASS  string
		INDEX string
	}

	cassandraConfig struct {
		DSN      string
		PORT     int
		USER     string
		PASS     string
		KEYSPACE string
	}
)
